variable "project" {
  type        = string
  default     = null
  description = "project name to provision bucket in"
}

variable "name" {
  type        = string
  default     = null
  description = "name of bucket"
}

variable "location" {
  type        = string
  default     = null
  description = "region where bucket will be created"
}

variable "force_destroy" {
  type        = bool
  default     = null
  description = "if true, it means you do not have to delete object in the bucket before deleting the bucket"
}

variable "storage_class" {
  type        = string
  default     = null
  description = "the default storage class of all objects stored in the bucket"
}

variable "uniform_bucket_level_access" {
  type        = bool
  default     = null
  description = "if true, all the objects will have the same level of access in the bucket"
}

variable "autoclass_bool" {
  type        = bool
  default     = null
  description = "if true autoclass is enabled"
}

# variable "terminal_storage_class" {
#   type        = string
#   default     = null
#   description = "the new storage class after the object has not been accessed after a specific period of time"
# }

variable "versioning_bool" {
  type        = bool
  default     = null
  description = "if true, every version of objects stored is kept"
}

variable "public_access_prevention" {
  type        = string
  default     = null
  description = "controls if the public can get or push objects to this bucket"
}

