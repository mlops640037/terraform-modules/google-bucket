resource "google_storage_bucket" "bucket_list" {
  project                     = var.project                     #"mlops-dev-406321"
  name                        = var.name                        #"mlops-first-terraform-123456-bucket"
  location                    = var.location                    #"us-central1"
  force_destroy               = var.force_destroy               #true
  storage_class               = var.storage_class               #"STANDARD"
  uniform_bucket_level_access = var.uniform_bucket_level_access #true
  autoclass {
    enabled = var.autoclass_bool #true
    #terminal_storage_class = var.terminal_storage_class #"NEARLINE"
  }
  #   lifecycle_rule {
  #     action {
  #       type          = "SetStorageClass"
  #       storage_class = "NEARLINE"
  #     }
  #     condition {
  #       age                   = 60
  #     }
  #   }

  versioning {
    enabled = var.versioning_bool #false
  }
  public_access_prevention = var.public_access_prevention #"enforced"
}

#with autoclass enabled, we do not need the lifecycle rule we wrote anymore 